// przykładowe operacje w js
// let wynik = 10*10;
// wynik = 500;
// console.log(wynik);

// const stala = 120;


// let name = prompt("Jak masz na imię?");
// console.log("Twoje imię to: " + name);

// alert("Hello World");

// let tab1 = [1, 2, 3];

// console.log(tab1[2])


// // for(let i in tab1) {
// //     console.log(tab1[i])
// // } alt:

// for(i of tab1) {
//     console.log(i)
// }

// tab1.push(5)

// for(let i in tab1) {
//     console.log(tab1[i])

// wyszukiwanie elementów
let mainElement = document.getElementById("main")
console.log(mainElement)

// let pElements = document.getElementsByTagName('p')
// console.log(pElements[0])

// let middleOne = document.getElementsByClassName('middle-one')
// console.log(middleOne[0].getElementsByTagName('div'))

// zmiana zawartości elementów
// let name = prompt('Jak masz na imię?')

// mainElement.innerText = "witaj dfg" + name

// Funkcje

// function log(logMessage) {
//     console.log(new Date + logMessage)
// }

// log("test message1")
// log("test message2")

// // funkcja jako parametr w innej funkcji

// function timeout() {
//     alert("alert")
// }

// setTimeout(timeout, 5000)

// // zapisy alternatywne(przydatne gdy funkcje wywolujemy raz)

// setTimeout(function() {
//     alert("czas minął")
// }, 10000);

// setTimeout(() => alert('czas minął znów'), 15000) 

// zachowania możemy ustawić w fokumencie HTML lub JS

document.getElementById('p1').onclick = () => {alert('zostaw to')}

// zdarzenia możemy definiować dla całego dokumetu
document.onclick = () => {console.log('nie klikaj tyle')}

// pobieranie danych z REST API i przykładowe wykorzystanie
let id = null;

let reload = () => {
    
    clearTimeout(id);
    $('#norris').slideUp(() => {
    fetch('https://api.chucknorris.io/jokes/random')
    .then((r) => r.json())
    .then(r => { $('#norris').html(r.value);
        $('#norris').slideDown()})

    id = setTimeout(reload, 10000);
    });
};


    reload ();
    setTimeout(reload, 10000)
// document.getElementById('button').onclick = reload;
$('#button').click(reload);