$(() => {
   $('.progress').each((i, e) => {
        let $e = $(e);
        let width = $e.data('percent');
        let strip = $e.find("div");
        strip.css("--bs-progress-bar-transition", 0);
        strip.css("width", '0%');

   })  

    let fired = false;

    let fillProgress = () => {
            if (
                !fired &&
                $(window).scrollTop() +$(window).height() > 
                $('#strips').offset().top
                ) {
                 fired = true;   
                 $('.progress').each((i, e) => {
                    let $e = $(e);
                    let width = $e.data('percent');
                    let strip = $e.find("div");
                        strip.css("transition", "1s");
                        strip.css("width", width + "%");
            });
        }
       }
    

    $(window).scroll(fillProgress);
    fillProgress();
});

